INTRODUCTION
------------

The LDAP User Roles Export module provides additional functionality
to the contributed LDAP modules, to export user roles to a LDAP.

Drupal user roles are being used for fine-grained authorization, however, the
contributed LDAP module does not facilitate the export of the user roles
themselves and also not the user role assignments defined in our Drupal setup.
This module implements the functionality to initiate a complete export of all
user roles and the role assignments to users themselves. When a role or role
assignment to a user is added, changed or deleted, the modification is
reflected in the LDAP.

REQUIREMENTS
------------

This module requires the following modules:

 * LDAP Authorization (https://www.drupal.org/project/ldap)
 * LDAP Servers (https://www.drupal.org/project/ldap)
 * LDAP User Module (https://drupal.org/project/ldap)
 * Chaos tools (https://www.drupal.org/project/ctools)
 * Entity API (https://www.drupal.org/project/entity)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer site configuration (System module)

     The LDAP Servers module uses this permission for the configuration of
     LDAP servers. In order to alter configuration parameters of the
     LDAP User Role Export module, the user has to have this
     permission.

 * Customize the settings per LDAP server via Configuration » LDAP
   Configuration » 2. Servers.

   Make sure the 'Groups are not relevant to this Drupal site' is not checked,
   and the 'Name of Group Object Class' and 'LDAP Group Entry Attribute
   Holding User's DN, CN, etc.' have been give correct values.

 * In addition to the configuration options of the LDAP Servers module, the
   LDAP User Roles Export module provides it's own administrative
   configuration pages that should have correct values: Configuration » LDAP
   Configuration » LDAP User Roles Export.

USAGE
-----

When a user role is created, renamed or deleted or when a user role is added to/
removed from a user, the change is automatically reflected to the LDAP. In order
to initiate a full export of a user roles and the assignment of the roles to
users, use the 'export user roles' operation (on the LDAP Server configuration
page: 'admin/config/people/ldap/servers').
