<?php
/**
 * @file
 * Admin settings for the LDAP User Roles Export module.
 */

/**
 * Page callback:
 *
 * @see ldapure_menu()
 *
 * @ingroup forms
 */
function ldapure_user_roles_export_settings_form($form, &$form_state) {
  $form = array();

  // Get all the LDAP servers that have grouping enabled.
  $ldap_servers = ldapure_server_load_enabled_with_grouping();

  // Show a message that no servers have been found with grouping enabled.
  if (empty($ldap_servers)) {
    $form['ldapure_groupdn_empty'] = array(
      '#type' => 'markup',
      '#markup' => t('No LDAP server configuration are available that have enabled grouping (on the LDAP server configuration page).'),
    );

  // When there are LDAP servers present that should have configuration for
  // grouping, provide input text fields for the the Group DN value.
  } else {
    $form['ldapure_groupdn_description'] = array(
      '#type' => 'markup',
      '#markup' => t('The expression should include <em>%rolename</em>, which will be replaced by the name of the user role. For instance: <em>cn=%rolename,ou=roles,dc=fortissimo,dc=eu</em>.'),
    );

    foreach ($ldap_servers as $ldap_server) {
      $form['ldapure_groupdn_server_' . $ldap_server->sid] = array(
        '#type' => 'textfield',
        '#title' => t('Group dn expression for %ldap_server_name', array('%ldap_server_name' => $ldap_server->name)),
        '#default_value' => variable_get('ldapure_groupdn_server_' . $ldap_server->sid, ''),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Page callback: Constructs the (admin) confirmation form to trigger the export
 * of the user roles themselves and the role assignments from the Drupal
 * environment to the LDAP server.
 *
 * @see ldapure_menu()
 *
 * @ingroup forms
 */
function ldapure_export_user_roles_confirm_form($form, &$form_state, $ldap_server) {
  // Use the confirm_form() function to generate a basic form.
  $form = confirm_form(
    $form,
    t('Export user roles data from Drupal to the LDAP server'),
    'admin/config/people/ldap/servers/list',
    t('Are you sure that you want to export as well as all user roles and also the role assignments from this Drupal environment to the LDAP server %ldap_machine_name?', array('%ldap_machine_name' => $ldap_server->name))
  );

  return $form;
}

/**
 * Form submission handler ldapure_export_user_roles_confirm_form().
 */
function ldapure_export_user_roles_confirm_form_submit($form, &$form_state) {
  // The object representing the LDAP server configuration/connection is
  // available via the $form_state variable (since it was passed as an argument
  // when constructing the confirmation form).
  $ldap_server = $form_state['build_info']['args'][0];

  // Redirect the user to the overview of LDAP servers, when the submission is
  // finished.
  $form_state['redirect'] = 'admin/config/people/ldap/servers/list';

  // Export all user roles and the users assigned to each role.
  if (ldapure_export_user_roles($ldap_server)) {
    drupal_set_message(t('All user roles have been exported to the LDAP server %ldap_machine_name, including the role assignments of all users.', array('%ldap_machine_name' => $ldap_server->name)));
  } else {
    drupal_set_message(t('Something might have gone wrong while exporting all user roles and the users assigned to each role. Please take a look at the <a href="@url">recent log messages</a> for more information.', array('@url' => url('admin/reports/dblog'))), 'warning');
  }
}
